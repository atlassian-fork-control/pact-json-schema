'use strict';

const testSchemaAgainstExamples = require('./support/test-schema-against-examples');

describe('pact-json-schema v2', () => {
    testSchemaAgainstExamples('schemas/v2/schema.json', 'test/examples/v2');
});
